<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3</title>
</head>
<body>
    <h1>Generador de Matrices</h1>
    <table border="1">
        <?php
        include 'funciones.php';

        const TAMANO_MATRIZ = 6;

        do {
            $matriz = generarMatriz(TAMANO_MATRIZ);
            $sumaDiagonal = calcularSumaDiagonal($matriz);

            echo "<tr><td colspan='" . TAMANO_MATRIZ . "'><strong>Matriz generada:</strong></td></tr>";
            foreach ($matriz as $fila) {
                echo "<tr>";
                foreach ($fila as $valor) {
                    echo "<td>$valor</td>";
                }
                echo "</tr>";
            }
            echo "<tr><td colspan='" . TAMANO_MATRIZ . "'><strong>Suma de la diagonal principal:</strong> $sumaDiagonal</td></tr>";

            if ($sumaDiagonal >= 10 && $sumaDiagonal <= 15) {
                echo "<tr><td colspan='" . TAMANO_MATRIZ . "'><strong>La suma de la diagonal principal está entre 10 y 15. Finalización del script.</strong></td></tr>";
                break;
            }
        } while (true);
        ?>
    </table>
</body>
</html>