<?php
function generarMatriz($n) {
    $matriz = array();
    $seed = mt_rand();
    mt_srand($seed);
    for ($i = 0; $i < $n; $i++) {
        $fila = array();
        for ($j = 0; $j < $n; $j++) {
            $fila[] = mt_rand(0, 9);
        }
        $matriz[] = $fila;
    }
    return $matriz;
}

function imprimirMatriz($matriz) {
    echo "<table border='1'>";
    foreach ($matriz as $fila) {
        echo "<tr>";
        foreach ($fila as $valor) {
            echo "<td>$valor</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}

function calcularSumaDiagonal($matriz) {
    $suma = 0;
    $n = count($matriz);
    for ($i = 0; $i < $n; $i++) {
        $suma += $matriz[$i][$i];
    }
    return $suma;
}
?>
